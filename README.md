# FolderGrid Appliance Docker Container #

## How to Get Started ##

 * Clone this project locally and launch the FolderGrid Appliance using [Docker Compose](https://docs.docker.com/compose/) with the included docker-compose.yml file. 
 * Subscribe to the [FolderGrid Appliance Support service](http://foldergrid.com/pricing.html) and we'll grant you access to the private repository holding the bitacuity/foldergrid container.